# lsh - your portable Linux shell

Running `not-linux` but want to run it?

`lsh` will let you do it, and manage your linux!

All of your standard files will be available in the linux container as well!

Install Docker: https://docs.docker.com/install/

Then clone the repo and put it in your $PATH
Run:

```bash
lsh
```

And you'll enter your linux container!
Note that this does not delete your host OS. No worries about data loss.

`lsh` is nice in that it does a few things for you:
- It automatically starts the container in the background, so entering is fast
- It creates only one container, so it will not use much disk space
- It mounts your home dir, so all of your files normally available are also available in the container
- And likewise, everything in your container's home dir is available on your computer as well!

`lsh` also comes with a few helpful flags as well!

```bash
$ lsh --help
lsh, the portable linux shell.

Run with no args to enter the shell.
Optional args:
--update        Update lsh script
--install       Build and set up your lsh image.
                This should be done automatically, but the option is provided if desired.
--uninstall     Uninstall lsh image.
                If you reinstall your image, it will not have the packages you previously installed.
--help          Print this help text and exit.
```

Note that `lsh` is configured to run with Ubuntu 18 and only a few utils.

If you want more utils, you can add to the Dockerfile, or `apt-get` them!

If you want a different Linux distro, just edit the Dockerfile!
